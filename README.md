# whack-a-mole

# update by Author : Syed Zubair
# Date : 16th Oct 2021 

## CLI Commands

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# serve jest test
npm test
```

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).
