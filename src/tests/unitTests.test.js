import { getRandomNumberWithinRange,isBoxVisibleInScreen,useCustomHook,getChangedResponseAfterTimeOut,getPosition} from './unitTests';
import { h } from "preact";

import { configure,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-preact-pure';
import { act } from 'react-dom/test-utils';

import "regenerator-runtime/runtime.js";

configure({ adapter: new Adapter });

test('test random number generated with innerHeight range', () => {
  expect(getRandomNumberWithinRange(window.innerHeight)).toBeLessThanOrEqual(window.innerHeight);
});

test('test random number generated with innerWidth range', () => {
  expect(getRandomNumberWithinRange(window.innerWidth)).toBeLessThanOrEqual(window.innerWidth);
});

test('test Box Width Visible In Screen', () => {
  expect(isBoxVisibleInScreen(window.innerWidth)).toBeTruthy();
});

test('test Box Height Visible In Screen', () => {
  expect(isBoxVisibleInScreen(window.innerHeight)).toBeTruthy();
});

describe('useCustomHook', () => {
  let results;
  const renderHook = hook => {
    function HookWrapper() {
      results = hook();
      return null;
    }
    mount(<HookWrapper />);
    return results;
  };

  it('test custom hooks', () => {
    renderHook(useCustomHook);
    expect(results.valueOfX).toEqual(0);

    act(() => {
      results.changeValueOfX();
    });

    expect(results.valueOfX).not.toBe(0);
  });
});

test('expect response to change after timeout duration', async () => {
    let result = getPosition();
    let timeoutDuration = 10000;
    expect(result.valueOfX).toBe(window.innerWidth);
    expect(result.valueOfY).toBe(window.innerHeight);
    getChangedResponseAfterTimeOut(timeoutDuration);
    await new Promise((r) => setTimeout(r, timeoutDuration));
    result = getPosition();
    expect(result.valueOfX).toBe(window.innerWidth/2);
    expect(result.valueOfY).toBe(window.innerHeight/2);
},30000);
