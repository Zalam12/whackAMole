import _ from "lodash";
import { useState } from "preact/hooks";

let positions = {valueOfX:window.innerWidth, valueOfY:window.innerHeight};

function getRandomNumberWithinRange(number){
  return _.random(number);
}

function isBoxVisibleInScreen(number){
  const maxLimit = number;
  number = getRandomNumberWithinRange(number);
  const boxSize = getRandomNumberWithinRange(number);
  if(number+boxSize>maxLimit) number=maxLimit-boxSize;

  if(number+boxSize>maxLimit) return false;
  return true;
}

function useCustomHook() {
  const [valueOfX, setX] = useState(0);
  const changeValueOfX = () => setX(1);
  return { valueOfX, changeValueOfX };
}

function getChangedResponseAfterTimeOut(idleTimeout){
  setTimeout(() => {
    positions = {valueOfX:window.innerWidth/2, valueOfY:window.innerHeight/2};
  }, idleTimeout);
}

function getPosition(){
    return positions;
}
  
  module.exports.getRandomNumberWithinRange = getRandomNumberWithinRange;
  module.exports.isBoxVisibleInScreen=isBoxVisibleInScreen;
  module.exports.getChangedResponseAfterTimeOut=getChangedResponseAfterTimeOut;
  module.exports.useCustomHook=useCustomHook;
  module.exports.getPosition=getPosition;
